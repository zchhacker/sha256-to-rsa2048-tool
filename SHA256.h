/****************************************************************
*
*  Filename:      .\TUOPU_IBS_E001_Download_Hex_Tool\SHA256.h
*  Project:       Exported definition of C-Array Flash-Driver
*  File created:  Tue Oct 24 09:04:04 2023

*
****************************************************************/

#ifndef ____
#define ____
#define _GEN_RAND 15086

#define _NUMBLOCKS 1

#define _DECRYPTVALUE	0x0
#define _DECRYPTDATA(a)   (unsigned char)((unsigned char)a ^ (unsigned char)_DECRYPTVALUE)
#define _BLOCK0_ADDRESS	0x800BC000
#define _BLOCK0_LENGTH	0x304020
#define _BLOCK0_CHECKSUM	0x5CD0u

V_MEMROM0 extern V_MEMROM1 unsigned char V_MEMROM2 Blk0[_BLOCK0_LENGTH];


#endif    /* #ifdef ____ */

