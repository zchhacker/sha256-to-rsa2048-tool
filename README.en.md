# SHA256ToRSA2048Tool

#### Description
一个读取.c文件中数组数据的程序
会读取数组末尾两行的hash值，并计算除rsa2048值
可编译成执行文件 传参（文件名，目标数组名）

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

Description:
Private Key Format: PKCS #8, e=65537, RSA2048
Signture: PKCS #1, SHA256 to RSA2048, Hex
1.Please store the hash value at the end of hex and convert the hex file to a C-Array file.
2.The program will read the last two rows of data (hash values 32Bytes) from the specified array in the specified file.
3.Create Output.hex to store encrypted data (256Bytes).
4.Online encrypted website URL:https://the-x.cn/cryptography/Rsa.aspx .
Program Usage:
SHA256_To_RSA2048_Tool.exe Filename TargetArrayName

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
