# SHA256ToRSA2048Tool

#### 介绍
一个读取.c文件中数组数据的程序
会读取数组末尾两行的hash值，并计算除rsa2048值
可编译成执行文件 传参（文件名，目标数组名）

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

Description:
Note:
Private Key Format: PKCS #8, e=65537, RSA2048
Signture: PKCS #1, SHA256 to RSA2048, Hex
1.Please store the hash value at the end of hex and convert the hex file to a C-Array file.
2.The program will read the last two rows of data (hash values 32Bytes) from the specified array in the specified file.
3.Create Output.hex to store encrypted data (256Bytes).
4.Online encrypted website URL:https://the-x.cn/cryptography/Rsa.aspx .
Program Usage:
SHA256_To_RSA2048_Tool.exe Filename TargetArrayName

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
