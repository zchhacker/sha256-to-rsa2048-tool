#ifndef _CONVERT_DATA_TYPE_H
#define _CONVERT_DATA_TYPE_H

void hex2string(char *hex,char *ascII,int len,int *newlen);
int string2hex(char* str,char* hex);
void TRACE(unsigned char *str, unsigned int len);

#endif //_CONVERT_DATA_TYPE_H
