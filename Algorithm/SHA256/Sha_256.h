/*============================================================================*/
/*
 *  file       < Sha256Calc.h >
 *  brief      < Compute SHA256 hash value head file >
 *
 *  < Compiler: don't care     MCU: don't care>
 *
 *  author     < NiKo.zheng >
 *  date       < 2020-03-14 >
 */
/*============================================================================*/

#ifndef _SHA_256_H
#define _SHA_256_H

/*=======[I N C L U D E S]====================================================*/
#include "..\Other\Std_Types.h"

#define RET_OK  			0u
#define RET_OVER_LIMIT 		1u

#define SHA256_LENGTH						32u

/*=======[T Y P E   D E F I N I T I O N S]====================================*/
/** return type for SecM module */
typedef uint8 SecM_Sha256_StatusType;

typedef struct
{
	uint8   Value[SHA256_LENGTH];
	uint32  DwordBufBytes;
	uint32  ByteNumLo;
	uint32  ByteNumHi;
	uint32  reg[8u]; /** h0 to h 7 -- old value store*/
	uint32  DwordBuf[16u]; /** data store */
	uint32  Padding[64u];
}Sha256Calc;

/** struct type for Crc */
typedef struct
{
    /* CRC buffer point */
    const uint8 *Sha256SourceBuffer;
    /* Sha256 length */
    uint16 Sha256ByteCount;
    /* current Sha256 value */
    Sha256Calc CurrentSha256;

} SecM_Sha256ParamType;

/*=======[E X T E R N A L   F U N C T I O N   D E C L A R A T I O N S]========*/
extern void Sha256Calc_init( Sha256Calc* t);

extern uint8 Sha256Calc_calculate( Sha256Calc* t, const uint8* dp, uint32 dl );


#endif
